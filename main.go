package main

import (
	"bytes"
	"crypto/rand"
	"database/sql"
	"fmt"
	"github.com/gin-gonic/gin"
	_ "github.com/mattn/go-sqlite3"
	"github.com/robfig/cron"
	"io/ioutil"
	"log"
	"math/big"
	"net/http"
	"time"
)

//取随机字符串
func CreateRandomString(len int) string {
	var container string
	var str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
	b := bytes.NewBufferString(str)
	length := b.Len()
	bigInt := big.NewInt(int64(length))
	for i := 0; i < len; i++ {
		randomInt, _ := rand.Int(rand.Reader, bigInt)
		container += string(str[randomInt.Int64()])
	}
	return container
}

func cronInit() {
	go func() {
		crontab := cron.New()
		crontab.AddFunc("0 20 8 * * *", deleteShortUrl) // 每天 8:20:00 定时执行 myfunc
		//		crontab.AddFunc("*/2 * * * * *", deleteShortUrl)
		crontab.Start()
	}()
}

func deleteShortUrl() {

	db, err := sql.Open("sqlite3", "./mk_short_url.db")
	if err != nil {
		fmt.Println("read fail", err)
	}
	//删除数据
	stmt, err := db.Prepare("delete from short_url where create_time<=? and url_type = temp")
	expireTime := time.Now().Unix() - 86400*7
	res, err := stmt.Exec(expireTime)
	affect, err := res.RowsAffected()
	fmt.Println(affect)
	defer db.Close()
}

func main() {

	cronInit()
	gin.SetMode(gin.ReleaseMode)
	//gin.SetMode(gin.DebugMode)
	db, err := sql.Open("sqlite3", "./mk_short_url.db")
	if err != nil {
		fmt.Println(err)
	}

	r := gin.Default()
	r.LoadHTMLGlob("templates/*")

	//生成页面
	r.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", gin.H{})

	})

	//生成短链方法
	r.POST("/creat", func(c *gin.Context) {
		longUrl := c.PostForm("long_url")
		urlType := c.PostForm("url_type")
		fmt.Println(urlType)
		if longUrl == "" {
			c.JSON(200, gin.H{
				"code": -1,
				"msg":  "链接不能为空",
				"data": "链接不能为空",
			})
			return
		}
		shortString := CreateRandomString(5)
		f, err := ioutil.ReadFile("1.txt")
		if err != nil {
			fmt.Println("read fail", err)
		}
		g_domain := string(f)
		createTime := time.Now().Unix()
		stmt, err := db.Prepare("INSERT INTO short_url(long_url, short_string, url_type, short_url, create_time) values(?,?,?,?,?)")
		res, err := stmt.Exec(longUrl, shortString, urlType, g_domain+"/u/"+shortString, createTime)
		if err != nil {
			fmt.Println(err)
		}
		id, err := res.LastInsertId()
		if err != nil {
			fmt.Println(err)
		}

		defer stmt.Close()
		fmt.Println(id)
		c.JSON(200, gin.H{
			"code": 1,
			"msg":  "生成成功",
			"data": g_domain + "/u/" + shortString,
		})

	})

	//访问
	r.GET("/u/:short", func(c *gin.Context) {
		if len(c.Param("short")) > 4 {
			//查询数据
			stmt, err := db.Prepare("select long_url from short_url where short_string = ?")
			if err != nil {
				log.Fatal(err)
			}
			defer stmt.Close()
			var long_url string
			err = stmt.QueryRow(c.Param("short")).Scan(&long_url)
			if err != nil {
				log.Fatal(err)
			}

			c.Redirect(http.StatusMovedPermanently, long_url)
		} else {
			c.JSON(200, gin.H{
				"code": 1,
				"msg":  "网址不正确",
				"data": "网址不正确",
			})
		}

	})

	r.Run(":80") // listen and serve on 0.0.0.0:8080

}
